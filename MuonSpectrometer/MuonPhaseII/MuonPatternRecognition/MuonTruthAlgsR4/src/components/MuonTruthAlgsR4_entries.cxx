

/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#include "../TruthSegmentMaker.h"
#include "../PrepDataToSimHitAssocAlg.h"

DECLARE_COMPONENT(MuonR4::TruthSegmentMaker)
DECLARE_COMPONENT(MuonR4::PrepDataToSimHitAssocAlg)